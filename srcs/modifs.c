#include "fdf.h"

/*
** int[0] = x, int[1] = y, int[2] = z
** quick explanation :
** zoom :
**    x = i * (fdf->offset + fdf->zoom);
** center before 3d rotation :
**    x -= (fdf->map_size_x + fdf->zoom * fdf->map_width) / 2;
** z changes according to zoom :
**    z += (z * fdf->zoom) / fdf->offset;
** deplacement :
**    point[0] += fdf->mov_x;
** center zoom :
**    point[0] -= ((fdf->map_size_x / 2) / fdf->offset) * fdf->zoom;
** center after 3d rotation :
**    point[0] += (fdf->map_size_x + fdf->zoom * fdf->map_width) / 2;
*/
int	*new_coordinates(t_fdf *fdf, int i, int j)
{
	int	x;
	int	y;
	int	z;
	int height;
	int	*point;

	point = ft_calloc(4, sizeof(int));
	x = i * (fdf->offset + fdf->zoom);
	y = j * (fdf->offset + fdf->zoom);
	x -= (fdf->map_size_x + fdf->zoom * fdf->map_width) / 2;
	y -= (fdf->map_size_y + fdf->zoom * fdf->map_height) / 2;
	z = fdf->map[j][i] * fdf->altitude;
	z += (z * fdf->zoom) / fdf->offset;
	height = Z_HEIGHT;
	if (height == 0)
		height = 1;
	z /= height;
	point[0] = x * cos(fdf->rad_x) + y * sin(fdf->rad_x);
	point[1] = y * cos(fdf->rad_x) - x * sin(fdf->rad_x);
	point[1] = point[1] * cos(fdf->rad_y) - -z * sin(fdf->rad_y);
	point[0] += fdf->mov_x;
	point[1] += fdf->mov_y;
	point[0] -= ((fdf->map_size_x / 2) / fdf->offset) * fdf->zoom;
	point[1] -= ((fdf->map_size_y / 2) / fdf->offset) * fdf->zoom;
	point[0] += (fdf->map_size_x + fdf->zoom * fdf->map_width) / 2;
	point[1] += (fdf->map_size_y + fdf->zoom * fdf->map_height) / 2;
	point[2] = z;
	point[3] = fdf->map[j][i];
	return (point);
}

void	position_state(t_fdf *fdf)
{
	char	*position;
	int		x;
	int		y;

	position = ft_strjoinfree(ft_strdup("zoom: "), ft_itoa(fdf->zoom));
	position = ft_strjoinfree(position, ft_strdup("  "));
	position = ft_strjoinfree(position, ft_strdup("deplacement: "));
	position = ft_strjoinfree(position, ft_itoa(fdf->mov_x));
	position = ft_strjoinfree(position, ft_strdup(" "));
	position = ft_strjoinfree(position, ft_itoa(fdf->mov_y));
	position = ft_strjoinfree(position, ft_strdup("  "));
	position = ft_strjoinfree(position, ft_strdup("rotation: "));
	position = ft_strjoinfree(position, ft_itoa(fdf->rot_x));
	position = ft_strjoinfree(position, ft_strdup(" "));
	position = ft_strjoinfree(position, ft_itoa(fdf->rot_y));
	x = fdf->img_size_x - 10 - ft_strlen(position) * 6;
	y = fdf->img_size_y - 10;
	mlx_string_put(fdf->mlx_ptr, fdf->win_ptr, x, y, 0xffffff, position);
	free(position);
}

/* 
** int	print_keycode(int keycode)
** {
** 	ft_putnbr(keycode);
** 	ft_putchar(' ');
** 	return (0);
** }
*/
