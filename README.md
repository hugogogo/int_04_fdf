# FDF - fil de fer (french for Wire)
---

This project is an introduction to 3D drawing. It takes a map file and render it with its height. It listen to keyboard events to apply transformations, such as translation, rotations, zoom, and change in altitude factor.

the input map is a simple text file that looks like this :

```
$> cat -e map_test.fdf
0  0  0  0  0$
0  0  0  0  0$
0 10  0  0  0$
0  0  0  0  0$
0  0  0  0  0$
$>
```

and the program converts it into a rectangular net, like a landscape with the height equal to the numbers :

![example 3D of map before modification](assets/fdf_demo_1.jpg)

we can modify the map to see a new 3D model :

```
$> cat -e map_test.fdf
0  0  0  0  0$
1  2  3  4  5$
0 10  8  0  0$
0  0  0  9  0$
0  0  0  0  0$
$>
```

![example 3D of map after modifications](assets/fdf_demo_2.jpg)

# ressources
---

- it is based on a library called minilibx : https://github.com/42Paris/minilibx-linux
- equations to transform a 2D drawing into a perspective : https://math.stackexchange.com/questions/2305792/3d-projection-on-a-2d-plane-weak-maths-ressources/2306853#2306853
- drawing a line between two points : https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
- bits manipulation for colors shifting : https://dev.to/freerangepixels/a-probably-terrible-way-to-render-gradients-1p3n
- optimizing events response with debouncing and throttling : https://stackoverflow.com/questions/25991367/difference-between-throttling-and-debouncing-a-function
- building images instead of asking the X library to draw each pixel one by one : https://github.com/Gontjarow/MiniLibX/blob/master/docs/mlx-tutorial-create-image.md

# transformations :
---

**zoom :**

![example 3D of map after modifications](assets/fdf_loop_zoom_element.gif)

**turn :**

![example 3D of map after modifications](assets/fdf_loop_turn_left_pyramide.gif)
![example 3D of map after modifications](assets/fdf_loop_turn_down_pylone.gif)

**move :**

![example 3D of map after modifications](assets/fdf_loop_move_down_tube.gif)

**altitude factor :**

![example 3D of map after modifications](assets/fdf_loop_change_height_50.gif)

