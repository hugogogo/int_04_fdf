
# - - - - - - - - - - - - - - #		name =	value \
#       variables names       #				value
# - - - - - - - - - - - - - - #		! name is case sensitive

NAME   = fdf

CC     = gcc

VPATH  = srcs

IDIR   = ./includes
_DEPS  = fdf.h
DEPS   = $(_DEPS:%.h=$(IDIR)/%.h)

LDIR   = ./libft
_LIBS  = libft.a
LIBS   = $(_LIBS:lib%.a=%)

SRCS   =	fdf.c \
			parse.c \
			modifs.c \
			draw.c \
			keypress.c

SRCS_B =	fdf.c \
			parse.c \
			modifs.c \
			keypress_bonus.c \
			draw_bonus.c

ODIR   = ./builds
OBJS   = $(SRCS:%.c=$(ODIR)/%.o)
OBJS_B = $(SRCS_B:%.c=$(ODIR)/%.o)

# flag -g generates debug informations, g3 is maximal version
CFLAGS = -I$(IDIR) -g3 -Wall -Wextra -Werror
LFLAGS = -L$(LDIR) -l$(LIBS)
CFLAGS += -I./minilibx-linux-master
LFLAGS += -lm -lmlx -lXext -lX11 -L./minilibx-linux-master


# - - - - - - - - - - - - - - #	target:	prerequisites	| $@ : target
#      rules to execute       #			recipe			| $< : 1st prerequisite
# - - - - - - - - - - - - - - #			recipe			| $^ : all prerequisites

all: $(NAME)

$(NAME): $(ODIR) $(OBJS) $(DEPS)
	make -C $(LDIR)
	$(CC) $(CFLAGS) -o $@ $(OBJS) $(LFLAGS)

bonus: fclean $(ODIR) $(OBJS_B) $(DEPS)
	make -C $(LDIR)
	$(CC) $(CFLAGS) -o fdf $(OBJS_B) $(LFLAGS)

$(ODIR):
	mkdir -p $@

$(ODIR)/%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

debug: CFLAGS += -fsanitize=address
debug: clean $(NAME)

leaks: $(NAME)
	valgrind --leak-check=full --show-leak-kinds=all ./$(NAME) maps/42_color.fdf
#	valgrind --leak-check=full --leak-resolution=low --show-reachable=yes ./$(NAME) maps/42_color.fdf

clean:
	/bin/rm -f $(OBJS) $(OBJS_B)

fclean: clean
	/bin/rm -rf $(ODIR)
	/bin/rm -f $(NAME)
	/bin/rm -rf a.out a.out.dSYM

libfclean:
	make fclean -C $(LDIR)

re: fclean all

relib: libfclean re

.PHONY: all clean fclean re gcc

