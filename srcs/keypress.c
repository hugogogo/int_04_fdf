#include "fdf.h"

int	keypress(int keycode, t_fdf *fdf)
{
	if (keycode == ESCAPE)
		shut_down(fdf);
	draw_image(fdf);
	return (0);
}
