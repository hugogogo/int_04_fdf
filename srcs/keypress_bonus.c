#include "fdf.h"

/*
** U -> up wiew
** I -> isometric original view
** O -> increase altitude
** P -> decrease altitude
** F -> fit to window
** C -> center
*/
void	keypress_more(int keycode, t_fdf *fdf)
{
	if (keycode == A)
		fdf->zoom += ZOOM;
	else if (keycode == W)
		fdf->zoom -= ZOOM;
	else if (keycode == U)
	{
		fdf->rot_x = 0;
		fdf->rot_y = 0;
	}
	else if (keycode == I)
	{
		fdf->rot_x = -45;
		fdf->rot_y = -35;
		fdf->mov_x = (fdf->win_size_x - fdf->map_size_x) / 2;
		fdf->mov_y = (fdf->win_size_y - fdf->map_size_y) / 2;
		fdf->zoom = 0;
	}
	else if (keycode == O)
		fdf->altitude += Z_HEIGHT_UP;
	else if (keycode == P)
		fdf->altitude -= Z_HEIGHT_DOWN;
}

int	should_ignore_keypress(const struct timeval *current_time, t_fdf *fdf)
{
	int	is_less;
	unsigned long	current_milliseconds;
	unsigned long	last_milliseconds;

	current_milliseconds = (current_time->tv_sec % 1000) * 1000 + current_time->tv_usec / 1000;
	last_milliseconds = (fdf->last_keypress_time.tv_sec % 1000) * 1000 + fdf->last_keypress_time.tv_usec / 1000;
	is_less = (current_milliseconds - last_milliseconds) < DEBOUNCE_TIME;

	if (!is_less)
		fdf->last_keypress_time = *current_time;
	return is_less;
}

/*
** Q -> move left
** D -> move right
** Z -> move up
** S -> move down
** A -> zoom
** W -> unzoom
*/
int	keypress(int keycode, t_fdf *fdf)
{
	struct timeval current_time;

	gettimeofday(&current_time, NULL);
	if (should_ignore_keypress(&current_time, fdf))
		return (0);
	if (keycode == ESCAPE)
		shut_down(fdf);
	else if (keycode == LEFT)
		fdf->rot_x += ROT;
	else if (keycode == RIGHT)
		fdf->rot_x -= ROT;
	else if (keycode == UP)
		fdf->rot_y += ROT;
	else if (keycode == DOWN)
		fdf->rot_y -= ROT;
	else if (keycode == Q)
		fdf->mov_x -= MOVE;
	else if (keycode == D)
		fdf->mov_x += MOVE;
	else if (keycode == Z)
		fdf->mov_y -= MOVE;
	else if (keycode == S)
		fdf->mov_y += MOVE;
	else
		keypress_more(keycode, fdf);
	draw_image(fdf);
	return (0);
}
