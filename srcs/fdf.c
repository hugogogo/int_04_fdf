#include "fdf.h"

int	shut_down(t_fdf *fdf)
{
	if (fdf->map)
	{
		while (fdf->map_height-- && fdf->map[fdf->map_height])
			free(fdf->map[fdf->map_height]);
		free(fdf->map);
	}
	if (fdf->img_ptr)
		mlx_destroy_image(fdf->mlx_ptr, fdf->img_ptr);
	if (fdf->win_ptr)
		mlx_destroy_window(fdf->mlx_ptr, fdf->win_ptr);
	if (fdf->mlx_ptr)
		mlx_destroy_display(fdf->mlx_ptr);
	if (fdf)
		free(fdf);
	exit(0);
	return (0);
}

void	init_server(t_fdf *fdf)
{
	fdf->mlx_ptr = mlx_init();
	if (!fdf->mlx_ptr)
		shut_down(fdf);
	fdf->win_ptr = mlx_new_window(fdf->mlx_ptr, fdf->win_size_x,
			fdf->win_size_y, "fdf");
	if (!fdf->win_ptr)
		shut_down(fdf);
	fdf->img_ptr = mlx_new_image(fdf->mlx_ptr, fdf->img_size_x,
			fdf->img_size_y);
	if (!fdf->img_ptr)
		shut_down(fdf);
	fdf->img_addr = mlx_get_data_addr(fdf->img_ptr, &(fdf->img_bpp),
			&(fdf->img_sizel), &(fdf->img_endian));
	if (!fdf->img_addr)
		shut_down(fdf);
}

void	init_offset(t_fdf *fdf)
{
	int	x;
	int	y;

	x = (fdf->map_width);
	y = (fdf->map_height);
	fdf->offset = fdf->win_size_x / ft_sqrt(x * x + y * y);
	if (!fdf->offset)
		fdf->offset = 1;
}

void	init_fdf(t_fdf *fdf)
{
	struct timeval current_time;

	fdf->win_size_x = 902;
	fdf->win_size_y = 913;
	fdf->img_size_x = fdf->win_size_x;
	fdf->img_size_y = fdf->win_size_y;
	init_offset(fdf);
	fdf->z_amplitude = fdf->max_z - fdf->min_z;
	fdf->altitude = 1;
	if (fdf->z_amplitude <= 10)
		fdf->altitude = 3;
	if (fdf->z_amplitude <= 5)
		fdf->altitude = 10;
	fdf->map_size_x = (fdf->map_width - 1) * fdf->offset + 1;
	fdf->map_size_y = (fdf->map_height - 1) * fdf->offset + 1;
	fdf->rot_x = -45;
	fdf->rot_y = -35;
	fdf->mov_x = (fdf->win_size_x - fdf->map_size_x) / 2;
	fdf->mov_y = (fdf->win_size_y - fdf->map_size_y) / 2;
	fdf->zoom = 0;
	gettimeofday(&current_time, NULL);
	fdf->last_keypress_time = current_time;
	init_server(fdf);
	draw_image(fdf);
}

int	usage() {
	write(2, "usage:\nneed one argument : ./fdf maps/<map>.fdf\n", 48);
	return (0);
}

int	main(int ac, char **av)
{
	t_fdf	*fdf;
	int		fd;

	if (ac != 2)
		return usage();
	fdf = malloc(sizeof(t_fdf));
	fd = open(av[1], O_RDONLY);
	if (!fd || !fdf)
		shut_down(fdf);
	fdf->map = parse_map(fdf, fd);
	init_fdf(fdf);
	mlx_hook(fdf->win_ptr, 2, 1L << 0, keypress, fdf);
	mlx_hook(fdf->win_ptr, 17, 1L << 17, shut_down, fdf);
	mlx_loop(fdf->mlx_ptr);
	return (0);
}

/*
**	x_event |   x_mask | action
**	      2 |  1L << 0 | key press
**	      3 |  1L << 1 | key release
**	      4 |          | mouse press
**	      5 |          | mouse release
**	      6 |          | mouse move
**	     12 |          | expose event
**	     17 | 1L << 17 | x button press (red button)
**	        |          |
**
** . math lib :
**    -lm         // needed at compilation to link the lib :
**                   gcc foo.c -o foo -lm
** . minilibx :
**    minilibx_opengl.tgz
**    minilibx_mms_20200219_beta.tgz
**    // to open an archive.tgz :
**       gzip -d archive.tgz     --> turn it into archive.tar
**       tar -xf archive.tar     --> un-archive it
**    // how to add a man directory to the manual :
**       . cp man/man1 /usr/local/share/man/man1
**         (create man1 if necessary)
**       . mandb
**   // i didn't use any of both library above but the one for linux :
**      https://github.com/42Paris/minilibx-linux
**      there are pbm with their man pages
*/
