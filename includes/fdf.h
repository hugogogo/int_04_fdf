#ifndef FDF_H
# define FDF_H

# include "../libft/includes/libft.h"
# include <mlx.h>
# include <math.h>			// for M_PI
# include <fcntl.h>			// for open
# include <time.h>			// for time
# include <sys/time.h>	// for gettimeofday

typedef struct s_fdf
{
	void						*mlx_ptr;
	void						*win_ptr;
	void						*img_ptr;
	char						*img_addr;
	int							**map;
	int							offset;
	int							min_z;
	int							max_z;
	int							z_amplitude;
	int							altitude;
	int							win_size_x;
	int							win_size_y;
	int							img_size_x;
	int							img_size_y;
	int							map_size_x;
	int							map_size_y;
	int							map_width;
	int							map_height;
	int							rot_x;
	int							rot_y;
	double					rad_x;
	double					rad_y;
	int							mov_x;
	int							mov_y;
	double					zoom;
	int							img_bpp;
	int							img_sizel;
	int							img_endian;
	struct timeval	last_keypress_time;
}				t_fdf;

// fdf.c
int		main(int ac, char **av);
void	init_fdf(t_fdf *fdf);
void	init_offset(t_fdf *fdf);
void	init_server(t_fdf *fdf);
int		shut_down(t_fdf *fdf);

// draw.c
void	draw_image(t_fdf *fdf);
void	draw_grid(t_fdf *fdf, int i, int j);
void	draw_lines(t_fdf *fdf, int *start, int *end);
void	draw_color_pixel(t_fdf *fdf, int new_x, int new_y, int z);
void	draw_pixel(t_fdf *fdf, int x, int y, int color);
int		is_outise_screen(t_fdf *fdf, int x, int y);
int		get_primary_color(char primar, int percent);
int		color_percent(t_fdf *fdf, int *p_start, int *p_end, int p_numerator, int p_denominator);

// keypress.c
void	keypress_more(int keycode, t_fdf *fdf);
int		keypress(int keycode, t_fdf *fdf);

// modifs.c
int		*new_coordinates(t_fdf *fdf, int i, int j);
void	position_state(t_fdf *fdf);
int		print_keycode(int keycode);

// parse.c
void	z_amplitude(t_fdf*fdf, int i);
int		**split_to_map(t_fdf *fdf, char *raw);
int		is_color(char *color);
void	size_map(t_fdf *fdf, char *raw, int height);
int		**parse_map(t_fdf *fdf, int fd);

// steps size for the height transform with p and o
// must be > 0
// 100 is small, 1 is the biggest
# define Z_HEIGHT				10
// then define the speed for height transform
// must be > 0
// 1 is small, 100 is big
# define Z_HEIGHT_UP		20
# define Z_HEIGHT_DOWN	20

/*
	- 50       : pink   - blue
	- elem     : purple - green
	- pylone   : brown  - purple
	- pyramide : white  - pink
	- semitube : green  - pink
	- t2       : white  - green
	- test     : white  - pink
*/
// color for altitude
# define COLOR_START		0xffffff // white
/*
# define COLOR_START		0xf263a6 // pink
# define COLOR_START		0xa263f6 // purple
# define COLOR_START		0x1423e6 // blue
# define COLOR_START		0x877264 // brown
# define COLOR_START		0x00d700 // green
*/
/*
# define COLOR_END			0x877264 // brown
# define COLOR_END			0x1423e6 // blue
# define COLOR_END			0xf263a6 // pink
# define COLOR_END			0xffffff // white
# define COLOR_END			0x00d700 // green
*/
# define COLOR_END			0xa263f6 // purple

// zoom change for each keypress
# define ZOOM						1

// degree rotation for each keypress
# define ROT						3

// pixels deplacement for each keypress
# define MOVE						10

// minimum time in milliseconds between two keypress
# define DEBOUNCE_TIME	50

# define ESCAPE		65307
# define UP				65362
# define DOWN			65364
# define LEFT			65361
# define RIGHT		65363
# define A				97
# define B				98
# define C				99
# define D				100
# define E				101
# define F				102
# define G				103
# define H				104
# define I				105
# define J				106
# define K				107
# define L				108
# define M				109
# define N				110
# define O				111
# define P				112
# define Q				113
# define R				114
# define S				115
# define T				116
# define U				117
# define V				118
# define W				119
# define X				120
# define Y				121
# define Z				122

#endif
