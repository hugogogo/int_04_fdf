#include "fdf.h"

void	draw_image(t_fdf *fdf)
{
	int	i;
	int	j;

	fdf->rad_x = fdf->rot_x * M_PI / 180;
	fdf->rad_y = fdf->rot_y * M_PI / 180;
	i = -1;
	while (++i < fdf->img_size_y * fdf->img_sizel)
		*(unsigned int *)(fdf->img_addr + i) = 0;
	j = -1;
	while (++j < fdf->map_height)
	{
		i = -1;
		while (++i < fdf->map_width)
			draw_grid(fdf, i, j);
	}
	mlx_put_image_to_window(fdf->mlx_ptr, fdf->win_ptr, fdf->img_ptr, 0, 0);
}

void	draw_grid(t_fdf *fdf, int i, int j)
{
	int	*point_start;
	int	*point_end;

	point_start = new_coordinates(fdf, i, j);
	point_end = NULL;
	if (i + 1 < fdf->map_width)
		point_end = new_coordinates(fdf, i + 1, j);
	draw_lines(fdf, point_start, point_end);
	if (j + 1 < fdf->map_height)
	{
		free(point_end);
		point_end = new_coordinates(fdf, i, j + 1);
	}
	draw_lines(fdf, point_start, point_end);
	free(point_start);
	free(point_end);
}

void	draw_lines(t_fdf *fdf, int *start, int *end)
{
	int	dx;
	int	dy;
	int	z;
	int	i;
	int	j;

	if (end)
	{
		dx = end[0] - start[0];
		dy = end[1] - start[1];
		i = 0;
		j = 0;
		z = start[2];
		while (ft_abs(i) <= ft_abs(dx) && ft_abs(j) <= ft_abs(dy))
		{
			if ((dx + dy) && (i + j))
				z = start[2] + (end[2] - start[2]) * (i + j) / (dx + dy);
			draw_color_pixel(fdf, start[0] + i, start[1] + j, z);
			if (!ft_abs(dx) || ft_abs(j) < ft_abs(i * dy / dx))
				j += ft_sign(dy);
			else
				i += ft_sign(dx);
		}
	}
}

void	draw_color_pixel(t_fdf *fdf, int new_x, int new_y, int z)
{
	int	color;

	color = 0xffffff;
	z -= (z * fdf->zoom) / fdf->offset;
	z /= fdf->altitude;
	if (z > fdf->min_z && fdf->z_amplitude)
	{
		color = color ^ (((0xff / fdf->z_amplitude) * (z - fdf->min_z)) << 16);
		color = color ^ (((0xff / fdf->z_amplitude) * (z - fdf->min_z)) << 0);
	}
	draw_pixel(fdf, new_x, new_y, color);
}

void	draw_pixel(t_fdf *fdf, int x, int y, int color)
{
	int	position;
	int	xmax;
	int	ymax;

	xmax = fdf->img_sizel / (fdf->img_bpp / 8);
	ymax = fdf->img_size_y;
	if (x < 0 || y < 0 || x > xmax || y > ymax)
		return ;
	position = y * fdf->img_sizel + x * fdf->img_bpp / 8;
	*(unsigned int *)(fdf->img_addr + position) = color;
}
